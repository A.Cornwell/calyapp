import './App.css';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import MainPage from './components/mainPage';
import AddMeetingForm from './components/addMeetingForm';
import AddTechAvailabilityForm from './components/addTechAvailabilityForm';
import LandingPage from './components/landingPage';
import LoginForm from './users/login';
import {CsrfTokenProvider} from './components/CsrfTokenProvider';
import TechAvailabilityList from './components/techAvailabilityList';

function App() {
  return (
    <BrowserRouter>
      <CsrfTokenProvider>
        <Routes>
          <Route path='/' element={<Navigate to='/home' />} />
          <Route path='/home' element={<LandingPage />} />
          <Route path='/main' element={<MainPage />} />
          <Route path='/addmeeting' element={<AddMeetingForm />} />
          <Route path='/addtech' element={<AddTechAvailabilityForm />} />
          <Route path='/login' element={<LoginForm />} />
          <Route path='/tech_list' element={<TechAvailabilityList />} />
        </Routes>
      </CsrfTokenProvider>
    </BrowserRouter>
  );
}

export default App;
