from django.contrib import admin
from .models import Tech, Meeting, TechAvailability

# Register your models here.


@admin.register(Tech)
class TechAdmin(admin.ModelAdmin):
    pass


@admin.register(Meeting)
class MeetingAdmin(admin.ModelAdmin):
    pass


@admin.register(TechAvailability)
class TechAvailabilityAdmin(admin.ModelAdmin):
    pass
