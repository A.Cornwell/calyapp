import React, { useState, useEffect } from "react";
import { eachDayOfInterval, parse, add, getDay, endOfMonth, endOfWeek, format, startOfToday, isToday, isSameMonth, isEqual, parseISO, isSameDay } from 'date-fns'
import { utcToZonedTime } from 'date-fns-tz'
import { Fragment } from 'react'
import {ChevronLeftIcon, ChevronRightIcon, EllipsisHorizontalIcon, HandThumbDownIcon, HandThumbUpIcon} from '@heroicons/react/20/solid'
import { Menu, Transition } from '@headlessui/react'
import AddMeetingForm from "./addMeetingForm";
import AddTechAvailabilityForm from "./addTechAvailabilityForm";
import Meeting from "./meeting";
import Logout from "../users/logout";
import baseUrl from "./apiConfig";


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function MainPage() {
    let today = startOfToday()
    let [selectedDay, setSelectedDay] = useState(today)
    let [currentMonth, setCurrentMonth] = useState(format(today, 'MMM-yyyy'))
    let firstDayCurrentMonth = parse(currentMonth, 'MMM-yyyy', new Date())
    const [showAddMeetingForm, setShowAddMeetingForm] = useState(false);
    const [showTechAvailabilityForm, setShowTechAvailabilityForm] = useState(false);
    const [meetings, setMeetings] = useState([]);
    const [techAvailabilities, setTechAvailabilities] = useState([]);


    const getTechAvailabilities = async () => {
      const response = await fetch(`${baseUrl}/maxim/tech_availability/`);

      if(response.ok) {
        const data = await response.json();
        setTechAvailabilities(data.tech_availabilities)
      }
    };

    useEffect(() => {
      getTechAvailabilities()
    }, []);



    const getMeetings = async () => {
      const response = await fetch(`${baseUrl}/maxim/meetings/`);

      if(response.ok) {
        const data = await response.json();

        setMeetings(data.meetings || []);  // Assuming the meetings array is inside the 'meetings' property
      }
    };

    useEffect(() => {
      getMeetings();
    }, []);


    let newDays = eachDayOfInterval ({
          start: firstDayCurrentMonth,
          end: endOfWeek(endOfMonth(firstDayCurrentMonth)),
    })


    function previousMonth() {
      let firstDayNextMonth = add(firstDayCurrentMonth, { months: -1 });
      setCurrentMonth(format(firstDayNextMonth, 'MMM-yyyy'));
    }


    function nextMonth() {
      let firstDayNextMonth = add(firstDayCurrentMonth, { months: 1 });
      setCurrentMonth(format(firstDayNextMonth, 'MMM-yyyy'));
    }


    function openAddMeetingForm() {
        setShowAddMeetingForm(true);
    }


    function closeAddMeetingForm() {
        setShowAddMeetingForm(false);
      }

    function openTechAvailabilityForm() {
      setShowTechAvailabilityForm(true);
    }

    function closeTechAvailabilityForm() {
      setShowTechAvailabilityForm(false);
    }

    const handleMeetingDelete = (meetingPk) => {
      setMeetings((prevMeetings) => prevMeetings.filter((meeting) => meeting.pk !== meetingPk));
    }

    const handleTechAavailabilityDelete = async (techId) => {

      const deleteUrl = `${baseUrl}/maxim/tech_availability/${techId}`;
      const deleteConfig = {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch(deleteUrl, deleteConfig);

        if (response.ok) {
          // If the DELETE request is successful, update the UI state
          setTechAvailabilities((prevAvailabilities) =>
            prevAvailabilities.filter((availability) => availability.id !== techId)
          );
        } else {
          // Handle errors or display a message if needed
          console.error('Failed to delete tech availability from the server');
        }
      } catch (error) {
        console.error('Error while deleting tech availability:', error);
      }
    };


    // Loop through meetings and convert start_date_time to Eastern Standard Time
    const meetingsWithEST = meetings.map((meeting) => {
      // Parse the start_date_time in UTC
      const startDatetimeUTC = parseISO(meeting.start_date_time);

      // Convert UTC to Eastern Standard Time (EST)
      const userTimezone = 'America/New_York'; // Eastern Standard Time
      const startDatetimeEST = utcToZonedTime(startDatetimeUTC, userTimezone);

      // Update the meeting object with the modified time
      return {
        ...meeting,
        startDatetimeEST: startDatetimeEST,
      };
    });

    // Filter meetings for the selected day
    let selectedDayMeetings = meetingsWithEST.filter((meeting) =>
      isSameDay(meeting.startDatetimeEST, selectedDay)
    );

    function combinedEvents(day) {
      const meetingsOnDay = meetingsWithEST.filter(meeting => isSameDay(meeting.startDatetimeEST, day));
      const techAvailabilitiesOnDay = techAvailabilities.filter(availability => isSameDay(parseISO(availability.date), day));

      return (
        <>
          <div className="mx-auto flex space-x-1">
              {meetingsOnDay.map(meeting => (
                <div key={`meeting-${meeting.pk}`} className="dot-spacing w-1.5 h-1.5 rounded-full bg-green-500"></div>
            ))}

          </div>
          <div className="mx-auto flex space-x-1">
              {techAvailabilitiesOnDay.map(availability => (
               <div key={`tech-availability-${availability.id}`} className="dot-spacing w-1.5 h-1.5 rounded-full bg-red-500"></div>
            ))}
          </div>
        </>

      );
    }


  return (
    <>
    <div className="lg:flex lg:h-full lg:flex-col">
    <header className="flex items-center justify-between border-b border-gray-200 px-6 py-4 lg:flex-none">
      <h1 className="text-base font-semibold leading-6 text-gray-900">
        {format(firstDayCurrentMonth, 'MMM yyyy')}
      </h1>
      <div className="flex items-center">
        <div className="relative flex items-center rounded-md bg-white shadow-sm md:items-stretch">
          <button
            type="button"
            onClick={previousMonth}
            className="flex h-9 w-12 items-center justify-center rounded-l-md border-y border-l border-gray-300 pr-1 text-gray-400 hover:text-gray-500 focus:relative md:w-9 md:pr-0 md:hover:bg-gray-50"
          >
            <span className="sr-only">Previous month</span>
            <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
          </button>

          <button
            type="button"
            onClick={nextMonth}
            className="flex h-9 w-12 items-center justify-center rounded-r-md border-y border-r border-gray-300 pl-1 text-gray-400 hover:text-gray-500 focus:relative md:w-9 md:pl-0 md:hover:bg-gray-50"
          >
            <span className="sr-only">Next month</span>
            <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
          </button>
        </div>
        <div className="hidden md:ml-4 md:flex md:items-center">
          <Menu as="div" className="relative">

            <Transition
              as={Fragment}
              enter="transition ease-out duration-100"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              {/* <Menu.Items className="absolute right-0 z-10 mt-3 w-36 origin-top-right overflow-hidden rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              </Menu.Items> */}
            </Transition>
          </Menu>

        </div>
        <Menu as="div" className="relative ml-6 md:hidden">
          <Menu.Button className="-mx-2 flex items-center rounded-full border border-transparent p-2 text-gray-400 hover:text-gray-500">
            <span className="sr-only">Open menu</span>
            <EllipsisHorizontalIcon className="h-5 w-5" aria-hidden="true" />
          </Menu.Button>

          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="absolute right-0 z-10 mt-3 w-36 origin-top-right divide-y divide-gray-100 overflow-hidden rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div className="py-1">
                <Menu.Item>
                    <button
                     onClick={openAddMeetingForm}
                     className="ml-3 px-4 py-1 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring focus:border-indigo-300"
                    >
                      Add Meeting
                    </button>

                </Menu.Item>
              </div>
              <div className="py-1">
                <Menu.Item>
                    <button
                     onClick={openTechAvailabilityForm}
                     className="ml-3 px-2 py-1 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring focus:border-indigo-300"
                    >
                      Add Availability
                    </button>

                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
    </header>
    <div className="shadow ring-1 ring-black ring-opacity-5 lg:flex lg:flex-auto lg:flex-col">
      <div className="grid grid-cols-7 gap-px border-b border-gray-300 bg-gray-200 text-center text-xs font-semibold leading-6 text-gray-700 lg:flex-none">
        <div className="bg-white py-2">
          S<span className="sr-only sm:not-sr-only">un</span>
        </div>
        <div className="bg-white py-2">
          M<span className="sr-only sm:not-sr-only">on</span>
        </div>
        <div className="bg-white py-2">
          T<span className="sr-only sm:not-sr-only">ue</span>
        </div>
        <div className="bg-white py-2">
          W<span className="sr-only sm:not-sr-only">ed</span>
        </div>
        <div className="bg-white py-2">
          T<span className="sr-only sm:not-sr-only">hu</span>
        </div>
        <div className="bg-white py-2">
          F<span className="sr-only sm:not-sr-only">ri</span>
        </div>
        <div className="bg-white py-2">
          S<span className="sr-only sm:not-sr-only">at</span>
        </div>
      </div>
        <div className="isolate grid w-full grid-cols-7 grid-rows-6 gap-px lg:hidden flex bg-gray-200 text-xs leading-6 text-gray-700 lg:flex-auto">
          {newDays.map((day, dayIdx) => (
            <button
              key={day.toString()}
              type="button"
              onClick={() => setSelectedDay(day)}
              className={classNames(
                dayIdx === 0 && colStartClasses[getDay(day)],
                isSameMonth(day, firstDayCurrentMonth) ? 'bg-white' : 'bg-gray-50',
                (isEqual(day, selectedDay) || isToday(day)) && 'font-semibold',
                isEqual(day, selectedDay) && 'text-white',
                !isEqual(day, selectedDay) && isToday(day) && 'text-red-500',
                !isEqual(day, selectedDay) && isSameMonth(day, firstDayCurrentMonth) && !isToday(day) && 'text-gray-900',
                !isEqual(day, selectedDay) && !isSameMonth(day, firstDayCurrentMonth) && !isToday(day) && 'text-gray-500',
                'flex h-14 flex-col px-3 py-2 hover:bg-gray-100 focus:z-10'
              )}
            >
              <time
                dateTime={format(day, 'yyyy-MM-dd')}
                className={classNames(
                  isEqual(day, selectedDay) && 'flex h-5 w-5 items-center justify-center rounded-full',
                  isEqual(day, selectedDay) && isToday(day) && 'bg-red-500',
                  isEqual(day, selectedDay) && !isToday(day) && 'bg-gray-900',
                  'ml-auto'
                )}
              >
                {format(day, 'd')}
              </time>
              {combinedEvents(day)}
            </button>
          ))}
        </div>
    </div>
    <section className="px-4 py-5 pb-5 sm:px-6 lg:hidden">
          <h2 className="pb-3 font-semibold text-gray-900 flex justify-center items-center">
            Schedule for {' '}
            <time className="ml-1" dateTime={format(selectedDay, 'yyyy-MM-dd')}>
                  {format(selectedDay, 'MMMM dd, yyy')}
            </time>
          </h2>
          <ol className="space-y-1 text-sm leading-6 text-gray-500">
          {techAvailabilities.map(availability => (
          isSameDay(parseISO(availability.date), selectedDay) && (
            <div key={`tech-availability-${availability.id}`} className="flex items-center justify-center space-x-2">
              <span
                className={`inline-block text-${
                  availability.available ? 'green-500' : 'red-500'
                }  text-center`}
              >
                {availability.tech.name} is {availability.available ? 'available' : 'not available'} today.
              </span>
              <button
                onClick={() => handleTechAavailabilityDelete(availability.id)}
                className="text-red-500 hover:text-red-700 focus:outline-none"
              >
                {availability.available ? (
                  <HandThumbUpIcon className="h-5 w-5 text-green-500" />
                ) : (
                <HandThumbDownIcon className="h-5 w-5 text-red-500" />
                )}
              </button>
            </div>
          )
        ))}
            {selectedDayMeetings.length > 0 ? (
              selectedDayMeetings.map((meeting) => (
              <Meeting meeting={meeting} key={meeting.pk} onDelete={handleMeetingDelete} />
            ))
            ):(
              <p className="flex justify-center items-center ">No meetings for today.</p>
            )}
          </ol>
    </section>
    <footer className="flex justify-center items-center pb-4 text-white text-sm">
      <Logout />
    </footer>
    {/* overlay for showing the AddMeetingForm */}
    {showAddMeetingForm && (
        <div className="fixed inset-0 z-50 flex items-center justify-center">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
          <div className="relative z-10 bg-white p-6 rounded-md shadow-md">
            <AddMeetingForm onClose={closeAddMeetingForm} onSuccess={getMeetings}  />
          </div>
        </div>
      )}
      {/* overlay for showing the AddTechAvailabilityForm */}
    {showTechAvailabilityForm && (
        <div className="fixed inset-0 z-50 flex items-center justify-center">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
          <div className="relative z-10 bg-white p-6 rounded-md shadow-md">
            <AddTechAvailabilityForm onClose={closeTechAvailabilityForm} onSuccess={getTechAvailabilities}  />
          </div>
        </div>
      )}
  </div>
  </>
  )
};

let colStartClasses = [
    '',
    'col-start-2',
    'col-start-3',
    'col-start-4',
    'col-start-5',
    'col-start-6',
    'col-start-7',
]
