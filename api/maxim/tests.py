from django.test import TestCase
from django.urls import reverse
from .models import Meeting


class ListTechViewTest(TestCase):
    def setup(self):
        self.tech_data = {
            "name": "Test Tech",
            "description": "This is a test tech",
        }

    def test_get_techs(self):
        url = reverse("list_techs")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn("techs", data)


class ListTechAvailabilityViewTest(TestCase):
    def setUp(self):
        self.tech_data = {
            "name": "Test Tech",
            "description": "This is a test",
        }

        self.tech_availability_data = {
            "tech": 1,
            "availability": "2024-01-01",
        }

    def test_get_tech_availabilities(self):
        # Test GET request to the view
        url = reverse("list_tech_availability")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn("tech_availabilities", data)


class ListMeetingViewTest(TestCase):
    def setUp(self):
        self.meeting_data = {
            "title": "Test Meeting",
            "date": "2024-01-01",
        }

    def test_get_meetings(self):
        url = reverse("list_meetings")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn("meetings", data)


class DeleteMeetingTest(TestCase):
    def setUp(self):
        # Create some initial data for testing
        self.meeting_data = {
            "name": "Test Meeting",
            "start_date_time": "2024-01-01",
            # Add other fields as needed
        }
        self.meeting = Meeting.objects.create(**self.meeting_data)

    def test_delete_meeting(self):
        url = reverse("show_meetings", args=[self.meeting.id])
        response = self.client.delete(url)

        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn("message", data)
        self.assertEqual(data["message"], "Meeting deleted successfully")

        # Ensure the meeting is deleted
        with self.assertRaises(Meeting.DoesNotExist):
            Meeting.objects.get(id=self.meeting.id)
