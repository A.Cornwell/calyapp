from .models import Tech, Meeting, TechAvailability
from .encoders import TechEncoder, MeetingEncoder, TechAvailabilityEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from datetime import datetime


@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_techs(request):
    if request.method == "GET":
        techs = Tech.objects.all()
        return JsonResponse({"techs": techs}, encoder=TechEncoder, safe=False)
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            tech = Tech.objects.create(**content)
            # Return a JsonResponse with a success message
            return JsonResponse({"message": "Tech created successfully", "tech": tech.id}, status=201)
        except Exception as e:
            # Return a JsonResponse with an error message and 500 status
            return JsonResponse({"error": str(e)}, status=500)


@csrf_exempt
@require_http_methods(["GET", "DELETE"])
def show_techs(request, pk):
    if request.method == "GET":
        techs = Tech.objects.get(id=pk)
        return JsonResponse(techs, encoder=TechEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Tech.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_meetings(request):
    if request.method == "GET":
        meetings = Meeting.objects.all()
        return JsonResponse(
            {"meetings": meetings},
            encoder=MeetingEncoder,
            safe=False
            )
    else:
        try:
            content = json.loads(request.body)
            meeting = Meeting.objects.create(**content)
            return JsonResponse(
                meeting,
                encoder=MeetingEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            response = JsonResponse({"message": "Invalid JSON format"})
            response.status_code = 400
            return response
        except Exception as e:
            response = JsonResponse({"message": f"Error: {str(e)}"})
            response.status_code = 500
            return response


@csrf_exempt
@require_http_methods(["GET", "DELETE", "PUT"])
def show_meetings(request, pk):
    if request.method == "GET":
        try:
            meeting = Meeting.objects.get(id=pk)
            return JsonResponse(
                meeting, encoder=MeetingEncoder, safe=False
            )
        except Meeting.DoesNotExist:
            response = JsonResponse({"message:" "Meeting does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            meeting = Meeting.objects.get(id=pk)
            meeting.delete()
            return JsonResponse(
                {"message": "Meeting deleted successfully"}
            )
        except Meeting.DoesNotExist:
            return JsonResponse(
                {"message:" "No delete, meeting doesn't exist"},
                status=404
            )
    else:
        try:
            content = json.loads(request.body)
            meeting = Meeting.objects.get(id=pk)
            props = ["notes"]
            for prop in props:
                if prop in content:
                    setattr(meeting, prop, content[prop])
            meeting.save()
            return JsonResponse(
                meeting, encoder=MeetingEncoder, safe=False
            )
        except Meeting.DoesNotExist:
            response = JsonResponse({"message:" "Does not exist"})
            response.status_code = 404
            return response


@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_tech_availability(request):
    if request.method == "GET":
        tech_availabilities = TechAvailability.objects.all()
        tech_availabilities_list = [TechAvailabilityEncoder().default(obj) for obj in tech_availabilities]
        return JsonResponse(
            {"tech_availabilities": tech_availabilities_list},
            encoder=TechAvailabilityEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            tech_id = content["tech"]
            tech = Tech.objects.get(id=tech_id)
            content["tech"] = tech
            tech_availability = TechAvailability.objects.create(**content)

            # Convert tech_availability to a dictionary
            tech_availability_dict = TechAvailabilityEncoder().default(
                tech_availability
            )

            return JsonResponse(
                tech_availability_dict, encoder=TechAvailabilityEncoder, safe=False
            )
        except Tech.DoesNotExist:
            response = JsonResponse({"message": "Invalid tech id"}, status=400)
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": f"No create tech availability. Error: {str(e)}"},
                status=400,
            )
            return response


@csrf_exempt
@require_http_methods(["GET", "DELETE", "PUT"])
def show_tech_availability(request, pk):
    if request.method == "GET":
        try:
            tech_availability = TechAvailability.objects.get(id=pk)
            return JsonResponse(
                tech_availability, encoder=TechAvailabilityEncoder, safe=False
            )
        except TechAvailability.DoesNotExist:
            response = JsonResponse({"message": "Tech availability does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            tech_availability = TechAvailability.objects.get(id=pk)
            tech_availability.delete()
            return JsonResponse(
                tech_availability,
                encoder=TechAvailabilityEncoder,
                safe=False,
            )
        except TechAvailability.DoesNotExist:
            return JsonResponse(
                {"message": "No delete, tech availability does not exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            tech_availability = TechAvailability.objects.get(id=pk)
            props = ["date", "available"]
            for prop in props:
                if prop in content:
                    setattr(tech_availability, prop, content[prop])
            tech_availability.save()
            return JsonResponse(
                tech_availability, encoder=TechAvailabilityEncoder, safe=False
            )
        except TechAvailability.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@csrf_exempt
@require_http_methods(["GET", "PUT"])
def show_tech_availability_tech_id(request, tech_id):
    tech = get_object_or_404(Tech, id=tech_id)
    tech_availabilities = TechAvailability.objects.filter(tech=tech)

    if request.method == "PUT":
        try:
            content = json.loads(request.body)
            availability_id = content.get("id")
            availability = get_object_or_404(TechAvailability, id=availability_id, tech=tech)

            # Update date if provided
            if "date" in content:
                # Convert the date string to a date object
                availability.date = datetime.strptime(content["date"], "%Y-%m-%d").date()
            # Update available if provided
            if "available" in content:
                availability.available = content["available"]

            availability.save()

            # Return the updated availability
            availability_dict = {
                "id": availability.id,
                "date": availability.date.strftime("%Y-%m-%d"),
                "available": availability.available,
                "tech": {
                    "id": availability.tech.id,
                    "name": availability.tech.name,
                }
            }

            return JsonResponse(availability_dict, safe=False)

        except TechAvailability.DoesNotExist:
            response = JsonResponse({"message": "Tech availability does not exist"})
            response.status_code = 404
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": f"Failed to update tech availability. Error: {str(e)}"},
                status=400,
            )
            return response

    # Manually convert TechAvailability objects to dictionaries
    tech_availabilities_list = [
        {
            "id": obj.id,
            "date": obj.date.strftime("%Y-%m-%d"),
            "available": obj.available,
            "tech": {
                "id": obj.tech.id,
                "name": obj.tech.name,
            }
        } for obj in tech_availabilities
    ]

    # Directly use JsonResponse without manual encoding
    return JsonResponse({"tech_availabilities": tech_availabilities_list}, safe=False)
