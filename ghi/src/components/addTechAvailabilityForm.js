import React, { useState, useEffect } from "react";
import { NavLink } from 'react-router-dom';
import baseUrl from "./apiConfig";


function AddTechAvailabilityForm({ onClose, onSuccess }) {
  const [successMessage, setSuccessMessage] = useState(null);
  const [techs, setTechs] = useState([]);
  const [formData, setFormData] = useState({
    date: "",
    available: "false",
    tech: "",
  });

  const getTechData = async () => {
    const techResponse = await fetch(`${baseUrl}/maxim/techs`);
    if (techResponse.ok) {
      const data = await techResponse.json();
      setTechs(data.techs);
    }
  };

  useEffect(() => {
    getTechData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();


    const techAvailabilityUrl =
      `${baseUrl}/maxim/tech_availability/`;

    const fetchConfigs = {
      method: "POST",
      body: JSON.stringify({
        ...formData,
        available: !!formData.available, // Convert to boolean
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const techAvailabilityResponse = await fetch(
      techAvailabilityUrl,
      fetchConfigs
    );
    if (techAvailabilityResponse.ok) {
      setFormData({
        date: "",
        available: "",
        tech: "",
      });
      setSuccessMessage("Availability saved successfully!");

    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;



    setFormData((prevData) => ({
      ...prevData,
      [inputName]: inputName === "tech" ? parseInt(value, 10) :
                    inputName === "available" ? value === "true" : value,
    }));

  };

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center">
      <div className="absolute inset-0 bg-gray-500 opacity-75"></div>

      <div className="relative z-10 bg-white p-6 rounded-md shadow-md">
      <NavLink to="/home" className="text-xs font-semibold leading-6 text-gray-800 mb-4 absolute top-0 left-0 mt-2 ml-2">
        Home
      </NavLink>
        <button
          type="button"
          onClick={() => onClose()}
          className="absolute top-0 right-1 p-1 m-1 text-gray-800 hover:text-gray-700 focus:outline-none"
        >
          X
        </button>
        <form
          onSubmit={handleSubmit}
          className="space-y-6 flex flex-col items-center"
        >
          {successMessage && (
            <div className="mt-4 bg-green-200 text-green-800 p-2 rounded-md mb-4">
              {successMessage}
            </div>
          )}
          <div className="w-full">
            <label
              htmlFor="date"
              className="block mt-4 text-sm font-medium text-gray-700"
            >
              Date
            </label>
            <input
              type="date"
              id="date"
              name="date"
              value={formData.date}
              onChange={handleFormChange}
              className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
            />
          </div>
          <div className="w-full">
            <label
              htmlFor="available"
              className="block text-sm font-medium text-gray-700"
            >
              Available
            </label>
            <select
              id="available"
              name="available"
              value={formData.available ? "true" : "false"} // Convert boolean to string
              onChange={handleFormChange}
              className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
            >
              <option value="">Select Availability</option>
              <option value="true">Yes</option>
              <option value="false">No</option>
            </select>
          </div>
          <div className="w-full">
          <label
              htmlFor="available"
              className="block text-sm font-medium text-gray-700"
            >
              Tech
            </label>
            <select
              value={formData.tech}
              onChange={handleFormChange}
              required
              name="tech"
              id="tech"
              className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
            >
                <option value="">Choose Tech</option>
              {techs.map((tech) => {
                return (
                  <option key={tech.id} value={tech.id}>
                    {tech.name}
                  </option>
                );
              })}
            </select>
          </div>
          <button
            type="submit"

            className="px-4 py-2 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring focus:border-red-300"
          >
            Save
          </button>
          <NavLink to="/tech_list">
            <button
            className="px-4 py-2 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring focus:border-red-300"
          >
            View Entries
          </button>
          </NavLink>

        </form>
      </div>
    </div>
  );
}

export default AddTechAvailabilityForm;
