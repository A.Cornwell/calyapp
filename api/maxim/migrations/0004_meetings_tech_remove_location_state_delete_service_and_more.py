# Generated by Django 4.0.3 on 2023-11-22 23:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("maxim", "0003_service"),
    ]

    operations = [
        migrations.CreateModel(
            name="Meetings",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("name", models.CharField(max_length=200)),
                ("start_date_time", models.DateTimeField()),
                ("notes", models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name="Tech",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=200)),
                ("available", models.BooleanField()),
            ],
        ),
        migrations.RemoveField(
            model_name="location",
            name="state",
        ),
        migrations.DeleteModel(
            name="Service",
        ),
        migrations.DeleteModel(
            name="Location",
        ),
        migrations.DeleteModel(
            name="State",
        ),
        migrations.AddField(
            model_name="meetings",
            name="tech",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="meetings",
                to="maxim.tech",
            ),
        ),
    ]
