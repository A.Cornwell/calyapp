import { useState, useEffect } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import baseUrl from "./apiConfig";


function TechAvailabilityList() {
  const [techs, setTechs] = useState([]);
  const [selectedTech, setSelectedTech] = useState(null);
  const [techAvailability, setTechAvailability] = useState([]);
  const [successMessage, setSuccessMessage] = useState('');
  const [selectedDropdownValue, setSelectedDropdownValue] = useState('');
  const navigate = useNavigate();

  const getTechData = async () => {
    const techResponse = await fetch(`${baseUrl}/maxim/techs`);
    if (techResponse.ok) {
      const data = await techResponse.json();
      setTechs(data.techs);
    }
  };

  const getTechAvailability = async () => {
    if (selectedTech) {
      try {
        const availabilityResponse = await fetch(
          `${baseUrl}/maxim/tech_availability/tech/${selectedTech.id}/`
        );
        if (availabilityResponse.ok) {
          const data = await availabilityResponse.json();
          setTechAvailability(data.tech_availabilities);
        } else if (availabilityResponse.status === 404) {
          console.log("Tech not found");

        } else {
          console.error(
            "Failed to fetch tech availabilities:",
            availabilityResponse
          );
        }
      } catch (error) {
        console.error("Error fetching tech availabilities:", error);
      }
    }
  };

  useEffect(() => {
    getTechData();
  }, []);

  useEffect(() => {
    getTechAvailability();
  }, [selectedTech]);

  const handleTechChange = (event) => {
    const selectedTechId = event.target.value;
    setSelectedDropdownValue(selectedTechId);
    const tech = techs.find((t) => t.id === parseInt(selectedTechId));
    setSelectedTech(tech);
  };


  const handleUpdate = async (availability) => {

    try {
        const response = await fetch(`${baseUrl}/maxim/tech_availability/${availability.id}/`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                available: availability.available,
            }),
          }
        );

        if (response.ok) {
            console.log("Tech Availability updated successfully!");
            setSuccessMessage("Updated successfully!");
            getTechAvailability();
            setTimeout(() => {
                setSuccessMessage("");
            }, 3000);
        } else {
            console.error("Failed to update tech availability:", response);
        }
    } catch (error) {
        console.error("Error updating tech availability:", error);
    }
};

  return (
    <div className="mt-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md mx-auto bg-white rounded-lg overflow-hidden">
        <div className="p-4">
        <NavLink to="/home" className="text-gray-600 font-semibold text-md mb-5">
           Home
        </NavLink>
          <h1 className="text-center text-xl font-bold mt-7 mb-3">Tech Availability List</h1>
          {successMessage && (
            <div className="bg-green-500 text-white p-2 mb-2 rounded-md">
              {successMessage}
            </div>
          )}
          <div className="mb-2">
            <select
              className="text-center mt-1 p-2 border rounded-md w-full focus:ring-2 focus:ring-red-500"
              onChange={handleTechChange}
              value={selectedTech?.id || ""}
            >
              <option value="">Select Tech</option>
              {techs.map((tech) => (
                <option key={tech.id} value={tech.id}>
                  {tech.name}
                </option>
              ))}
            </select>
          </div>

          {selectedTech && (
            <div>
              <h2 className="text-center text-lg font-semibold mt-6 mb-6">Logged availabilities for {selectedTech.name}</h2>
              <table className="min-w-full border rounded-md overflow-hidden text-sm">
                <thead className="bg-gray-200">
                  <tr>
                    <th className="py-2 px-2 text-left">Date</th>
                    <th className="py-2 px-2 text-left">Available</th>
                    <th className="py-2 px-2 text-left">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {techAvailability.map((availability) => (
                    <tr key={availability.id}>
                      <td className="py-2 px-2">{availability.date}</td>
                      <td className="py-2 px-6">
                        <select
                        value={availability.available ? "true" : "false"}
                        onChange={(e) =>
                            handleUpdate({
                                ...availability,
                                available: e.target.value === "true",
                            })
                        }
                        >
                            <option value="true">Yes</option>
                            <option value="false">No</option>
                        </select>
                        </td>
                      <td className="py-2 px-2">
                      <button
                        className="bg-red-500 text-white rounded-md ml-2 px-1 py-1"
                        onClick={() => {
                            handleUpdate(availability);

                        }}
                        >
                        Save
                      </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default TechAvailabilityList;
