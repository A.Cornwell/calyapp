import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, NavLink } from 'react-router-dom';
import baseUrl from '../components/apiConfig';

function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [csrfToken, setCsrfToken] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const getToken = async () => {
      try {
        const response = await axios.get(`${baseUrl}/user_auth/get-csrf-token/`);

        const csrfTokenFromHeaders = response.headers['set-cookie'];
        const csrfToken = csrfTokenFromHeaders.split(';')[0].split('=')[1];
        setCsrfToken(csrfToken);
      } catch (error) {
        console.error('Error fetching token:', error);
      }
    };
    getToken();
  }, []);

  const handleLogin = async () => {
    try {
      const response = await axios.post(
        `${baseUrl}/user_auth/login`,
        {
          email: email,
          password: password,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken,
          },
        }
      );

      console.log('Response Data:', response.data);

      if (response.status === 200) {
        console.log('Login successful');
        navigate('/main');
      } else {
        console.error('Login failed', response.data);
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  };


  return (
    <>
      <div className="relative max-w-md mx-auto mt-10">
        <NavLink to="/home"
          className="absolute top-2 right-2 text-sm text-gray-500 cursor-pointer"
        >
          Close
        </NavLink>
        <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="Date"
            >
              Email Address
            </label>
            <input
              type="email"
              id="email"
              name="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              placeholder="example@example.com"
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="Date"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              placeholder="password"
            />
          </div>
          <div className="flex items-center justify-center">
            <button
              onClick={handleLogin}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Login
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default LoginForm;
