import React, { useState } from "react";
import baseUrl from "./apiConfig";

function AddMeetingForm({ onClose, onSuccess }) {
    const [formData, setFormData] = useState({
        name: '',
        start_date_time: '',
        notes: '',
    });


    const handleSubmit = async (event) => {
        event.preventDefault();


        const meetingUrl = `${baseUrl}/maxim/meetings/`;

        const fetchConfigs = {

            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const meetingResponse = await fetch(meetingUrl, fetchConfigs);

        if (meetingResponse.ok) {
          onSuccess();
            setFormData({
                name: '',
                start_date_time: '',
                notes: ''
            });
        }
    };

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });


    };

    return (
        <div className="fixed inset-0 z-50 flex items-center justify-center">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>

          <div className="relative z-10 bg-white p-6 rounded-md shadow-md">
            <button
              type="button"
              onClick={() => onClose()}
              className="absolute top-0 right-0 p-1 m-1 text-gray-500 hover:text-gray-700 focus:outline-none"
            >
              X
            </button>
            <form onSubmit={handleSubmit} className="space-y-4 flex flex-col items-center">
              <div className="w-full">
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  Name
                </label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  value={formData.name}
                  onChange={handleFormChange}
                  className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
                />
              </div>
              <div className="w-full">
                <label htmlFor="start_date_time" className="block text-sm font-medium text-gray-700">
                  Date
                </label>
                <input
                  type="datetime-local"
                  id="start_date_time"
                  name="start_date_time"
                  value={formData.start_date_time}
                  onChange={handleFormChange}
                  className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
                />
              </div>
              <div className="w-full">
                <label htmlFor="notes" className="block text-sm font-medium text-gray-700">
                  Notes
                </label>
                <textarea
                  id="notes"
                  name="notes"
                  value={formData.notes}
                  onChange={handleFormChange}
                  rows="3"
                  className="mt-1 p-2 border border-gray-300 rounded-md w-full focus:outline-none focus:ring-2 focus:ring-red-500 focus:border-red-500"
                ></textarea>
              </div>
              <button
                type="submit"
                className="px-4 py-2 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring focus:border-red-300"
              >
                Save
              </button>
            </form>
          </div>
        </div>
      );

}
export default AddMeetingForm;
