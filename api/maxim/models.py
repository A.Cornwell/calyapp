from django.db import models


class Tech(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    # def get_api_url(self):
    #     return reverse("show_techs", kwargs={"pk": self.id})


class Meeting(models.Model):
    name = models.CharField(max_length=200)
    start_date_time = models.DateTimeField()
    notes = models.TextField()

    def __str__(self):
        return self.name

    # def get_api_url(self):
    #     return reverse("show_meetings", kwargs={"pk": self.id})


class TechAvailability(models.Model):
    date = models.DateField()
    available = models.BooleanField()

    tech = models.ForeignKey(
        Tech, related_name="tech_availabilities", on_delete=models.CASCADE
    )

    def __str__(self):
        availability_status = "Avail" if self.available else "Not Available"
        return f"{self.date} - {availability_status}"
