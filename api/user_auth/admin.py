from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    list_display = ("id",
                    "email",
                    "username",
                    "is_active",
                    "is_superuser",
                    "is_staff"
                    )


admin.site.register(CustomUser, CustomUserAdmin)
