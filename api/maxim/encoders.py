from .models import Tech, Meeting, TechAvailability
from common.json import ModelEncoder
from datetime import date


class TechEncoder(ModelEncoder):
    model = Tech
    properties = ["id", "name"]


class MeetingEncoder(ModelEncoder):
    model = Meeting
    properties = ["pk", "name", "start_date_time", "notes"]

    encoders = {"tech": TechEncoder()}


class TechAvailabilityEncoder(ModelEncoder):
    model = TechAvailability
    properties = [
        "id",
        "date",
        "available",
        "tech",
    ]

    encoders = {"tech": TechEncoder()}

    def default(self, obj):
        if isinstance(obj, date):
            # if obj is a date object, convert it to a string
            return obj.strftime("%Y-%m-%d")

        return super().default(obj)
