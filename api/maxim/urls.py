from django.urls import path
from .views import (
    list_techs,
    show_techs,
    show_meetings,
    list_meetings,
    list_tech_availability,
    show_tech_availability,
    show_tech_availability_tech_id,
)


urlpatterns = [
    path("techs/", list_techs, name="list_techs"),
    path("techs/<int:pk>", show_techs, name="show_techs"),
    path("meetings/<int:pk>/", show_meetings, name="show_meetings"),
    path("meetings/", list_meetings, name="list_meetings"),
    path("tech_availability/",
         list_tech_availability,
         name="list_tech_availability"),
    path(
        "tech_availability/<int:pk>/",
        show_tech_availability,
        name="show_tech_availability"),
    path(
        "tech_availability/tech/<int:tech_id>/",
        show_tech_availability_tech_id,
        name="show_tech_availability_tech_id"),
]
