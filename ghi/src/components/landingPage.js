import React from 'react'
import { NavLink } from 'react-router-dom';
import {ArrowLongRightIcon} from '@heroicons/react/20/solid'
import backgroundImage from '../images/background.jpg'

function LandingPage() {
    const backgroundStyle = {
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        minHeight: '100vh',
    }
    return (
        <>
        <nav className='text-white px-6 py-3 flex justify-between items-center bg-red-500'>
            <div className='ml-2 logo-2'>
                Caly
            </div>
            <ul className='flex items-center'>
                <li className='flex items-center '>
                    <NavLink to='/login' className="text-lg font-semibold leading-6 text-white flex items-center">
                        <p className='mr-0 mb-1'>Login</p>
                        <ArrowLongRightIcon className="h-5 w-5 text-white" />
                    </NavLink>
                </li>
            </ul>
        </nav>
        <div style={backgroundStyle} className="h-screen flex flex-col justify-center items-center">
            <div className='logo circle mx-auto bg-red-500 flex justify-center items-center'>
                Caly
            </div>
            <p className='mt-4 text-lg font-semibold'>Welcome!</p>
            <p className='mt-1 text-lg font-semibold'>If you have not been provided a log in please</p>
            <p className='mt-1 text-lg font-semibold'>click <NavLink to='/addtech' className="text-blue-600">here</NavLink> to log your availability.</p>
        </div>
        </>
    )
} export default LandingPage;
