import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import baseUrl from '../components/apiConfig';

function Logout() {
    const [csrfToken, setCsrfToken] = useState(null);
    const navigate = useNavigate();

    const handleLogout = async () => {
        try {
          const response = await axios.post(
            `${baseUrl}/user_auth/logout`,
            {},
            {
              headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrfToken,
              },
            }
          );

          console.log('Response Data:', response.data);

          if (response.status === 200) {
            console.log('Logout successful');
            navigate('/login'); // Redirect to login page after logout
          } else {
            console.error('Logout failed', response.data);
          }
        } catch (error) {
          console.error('Error during logout:', error);
        }
      };

    return (
        <div>
            <button
            onClick={handleLogout}
            className="ml-3 px-2 py-1 text-white bg-red-500 rounded-md hover:bg-red-700 focus:outline-none focus:ring">
                <p className=''>Logout</p>
            </button>
        </div>
      )
}
export default Logout;
