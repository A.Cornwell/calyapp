import React, { useState, Fragment } from "react";
import { Menu, Transition } from '@headlessui/react'
import {EllipsisHorizontalIcon, ClockIcon} from '@heroicons/react/20/solid'
import { utcToZonedTime } from "date-fns-tz";
import { parseISO } from "date-fns";
import baseUrl from "./apiConfig";


function Meeting({meeting, onDelete}) {
  const [showNotes, setShowNotes] = useState(false);
  const [successMessage, setSuccessMessage] = useState(" ");
  const [updatedNotes, setUpdatedNotes] = useState(meeting.notes || "");

  const startDatetimeUTC = parseISO(meeting.start_date_time);

  const userTimezone = 'America/New_York';
  const startDatetimeEST = utcToZonedTime(startDatetimeUTC, userTimezone)

  const timeString = startDatetimeEST.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', timeZone: userTimezone })



  const openNotes = () => {
    setShowNotes(true);
  }

  const closeNotes = () => {
    setShowNotes(false);
  }

  const handleDelete = async () => {
    const url = `${baseUrl}/maxim/meetings/${meeting.pk}/`;
    const fetchConfigs = {
      method:"DELETE",
      header: {
        "Content-Type": "application/json"
      }
    }
    const response = await fetch(url, fetchConfigs)
    const data = await response.json();
    onDelete(meeting.pk)
  }


  const handleUpdate = async () => {
    try {
      const response = await fetch(`http://localhost:8000/maxim/meetings/${meeting.pk}/`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            notes: updatedNotes,
          }),
        }
      );

      if (response.ok) {
        console.log("Notes updated successfully!");
        setSuccessMessage("Notes updated successfully!");
        setTimeout(() => {
          setSuccessMessage(" ");
        }, 3000);
      } else {
        console.error("Failed to update notes:", response);
      }
    } catch (error) {
      console.error("Error updating notes:", error);
    }
  };


  return (
    <>
      <div className="px-4 py-4 sm:px-6 lg:hidden relative">
        <ol className="rounded-lg bg-white text-sm shadow ring-1 ring-black ring-opacity-5">
          <li
            key={meeting.pk}
            className="group flex p-4 pr-6 focus-within:bg-gray-50 hover:bg-gray-50"
          >
            <div>
              <p className="font-semibold pb-1 text-gray-900">{meeting.name}</p>
              <time className="mt-2 flex items-center text-gray-700">
                <ClockIcon className="mr-2 h-5 w-5 text-gray-400" aria-hidden="true" />
                {timeString}
              </time>
            </div>
            <Menu
              as="div"
              className="mt-2 ml-auto relative focus-within:opacity-100 group-hover:opacity-100"
            >
              <div>
                <Menu.Button className="m-2 flex items-center rounded-full p-3 text-gray-500 hover:text-gray-600">
                  <span className="sr-only">Open options</span>
                  <EllipsisHorizontalIcon className="h-6 w-6" aria-hidden="true" />
                </Menu.Button>
              </div>
              <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <div className="absolute right-4 top-0 mt-8 mr-12">
                  <Menu.Items className="origin-top-left absolute w-36 mt-2 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="py-1">
                      <Menu.Item>
                        <button
                          onClick={openNotes}
                          className="block w-full text-left px-4 py-2 text-sm text-gray-800 hover:bg-gray-100 focus:outline-none"
                        >
                          View Notes
                        </button>
                      </Menu.Item>
                      <Menu.Item>
                        <button
                          onClick={handleDelete}
                          className="block w-full text-left px-4 py-1 text-sm text-red-600 hover:bg-red-100 focus:outline-none"
                        >
                          Delete
                        </button>
                      </Menu.Item>
                    </div>
                  </Menu.Items>
                </div>
              </Transition>
            </Menu>
          </li>
        </ol>
      </div>
      {showNotes && (
        <div className="fixed inset-0 z-50 flex items-center justify-center">
          <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
          <div className="relative z-10 bg-white p-6 rounded-md shadow-md">
            <textarea
              value={updatedNotes}
              onChange={(e) => setUpdatedNotes(e.target.value)}
              className="w-full h-32 mb-4 border rounded-md p-2"
            />
            <button
              onClick={handleUpdate}
              className="bg-red-500 text-white rounded-md px-3 py-1 mr-2"
            >
              Save
            </button>
            <button
              type="button"
              onClick={closeNotes}
              className="bg-gray-200 text-gray-800 rounded-md px-3 py-1"
            >
              Close
            </button>
            {successMessage && (
              <div className="mt-2 text-green-500">{successMessage}</div>
            )}
          </div>
        </div>
      )}
    </>
  );
}
export default Meeting;
