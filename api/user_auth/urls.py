from django.urls import path
from . import views
from .views import get_csrf_token

urlpatterns = [
    path('change-password', views.ChangePassword.as_view(), name='change_password'),
    path("register", views.UserRegister.as_view(), name="register"),
    path("login", views.UserLogin.as_view(), name="login"),
    path("logout", views.UserLogout.as_view(), name="logout"),
    path("user", views.UserView.as_view(), name="user"),
    path("get-csrf-token/", get_csrf_token, name="get_csrf_token"),
]
