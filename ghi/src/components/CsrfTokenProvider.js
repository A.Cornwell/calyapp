import React, { createContext, useContext, useEffect, useState } from 'react';
import axios from 'axios';
import baseUrl from './apiConfig';


const CsrfTokenContext = createContext();

export const CsrfTokenProvider = ({ children }) => {
  const [csrfToken, setCsrfToken] = useState(null);


  useEffect(() => {
    const fetchCsrfToken = async () => {
      try {
        const response = await axios.get(`${baseUrl}/user_auth/get-csrf-token/`, {
            withCredentials:true,
        });

        const csrfToken = response.data.csrf_token;

        setCsrfToken(csrfToken);
      } catch (error) {
        console.error('Error fetching CSRF token:', error);
      }
    };

    fetchCsrfToken();
  }, []);



  return (
    <CsrfTokenContext.Provider value={{ csrfToken }}>
      {children}
    </CsrfTokenContext.Provider>
  );
};

export const useCsrfToken = () => {
  const context = useContext(CsrfTokenContext);
  if (!context) {
    throw new Error('useCsrfToken must be used within a CsrfTokenProvider');
  }
  return context;
};
